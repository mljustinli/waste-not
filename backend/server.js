const API_PORT = 8080;

const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();

const mongoose = require("mongoose");
const mongo_uri = "mongodb://localhost/react-auth";
const User = require("./models/User.js");

mongoose.connect(mongo_uri, function(err) {
    if (err) {
        throw err;
    } else {
        console.log(`Successfully connected to ${mongo_uri}`);
    }
});

app.use(express.static(path.join(__dirname, "build")));

//important!!!
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.get("/ping", function(req, res) {
//     return res.send("pong");
// });

// app.get("/", function(req, res) {
//     res.sendFile(path.join(__dirname, "build", "index.html"));
// });

app.get("/api/home", function(req, res) {
    res.send("Welcome!");
});

app.get("/api/food", function(req, res) {
    res.send("Mango smoothie!");
});

// POST route to register a user
app.post("/api/register", function(req, res) {
    const { email, password, foodItems } = req.body;
    const user = new User({ email, password, foodItems });
    user.save(function(err) {
        if (err) {
            res.status(500).send(
                "Error registering new user please try again."
            );
        } else {
            res.status(200).send("Welcome to the club!");
        }
    });
});

app.listen(process.env.PORT || API_PORT, () =>
    console.log(`Listening on port ${API_PORT}`)
);
