const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");

const saltRounds = 10;

const FoodSchema = new Schema({
    name: { type: String, required: true },
    expDate: { type: Date, default: Date.now }
});

const UserSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    foodItems: [FoodSchema]
});

UserSchema.pre("save", function(next) {
    // Check if document is new or a new password has been set
    if (this.isNew || this.isModified("password")) {
        // Saving reference to this because of changing scopes
        const document = this;
        bcrypt.hash(document.password, saltRounds, function(
            err,
            hashedPassword
        ) {
            if (err) {
                next(err);
            } else {
                document.password = hashedPassword;
                next();
            }
        });
    } else {
        next();
    }
});

module.exports = mongoose.model("User", UserSchema);
