# Waste Not - The food waste management app!

## \*\*This is app is still a work in progress, but here's what it's about\*\*

### What is this for?
Well, I was tired of forgetting about leftovers and it's especially hard keeping track of what we have in the apartment with 5 other roommates.

I wanted a way to remind myself that I had certain leftovers still in the fridge, or even certain ingredients that I should use soon.

### What about Food Waste?
It seems that a large amount of food waste comes from thrown away leftovers, so maybe this app can serve to raise awareness about the issue of food waste as well.

The app will send you notifications if a food item or ingredient is close to the expiration date, and it can even remind you about ingredients you have at certain times during the day, such as before you prepare food.

### What is this made in?
- MongoDB
- Express
- ReactJS
- NodeJS

At the moment the repository is looking a little sparse since I'm focusing on the UI/UX design first. Once I have the React frontend complete, I'll dive into the Node backend for server communication!
