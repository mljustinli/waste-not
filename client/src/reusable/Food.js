import React, { Component } from "react";

class FoodList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "Loading..."
        };
    }

    componentDidMount() {
        fetch("/api/food")
            .then(res => res.text())
            .then(res => this.setState({ message: res }));
    }

    render() {
        const listData = this.getUserListData();
        const listItems = listData.map(item => (
            <FoodItem
                foodName={item.foodName}
                expDate={item.expDate}
                key={item.foodName + item.expDate.toString()}
            ></FoodItem>
        ));

        return (
            <div id="list-wrapper">
                <div> {this.state.message} </div>
                {listItems}
                <AddFoodItem />
            </div>
        );
    }

    getUserListData() {
        return [
            { foodName: "Pho", expDate: "February 19, 2019" },
            { foodName: "Cheese", expDate: "February 22, 2019" },
            { foodName: "Leftover potato salad", expDate: "March 2, 2019" }
        ];
    }
}

class FoodItem extends Component {
    render() {
        return (
            <div className="food-item-wrapper">
                <div className="check-icon-circle">
                    {/*<img src={CheckIcon} alt="Check Icon" className = "check-icon"/>*/}
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="80%"
                        height="80%"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="3"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-check"
                    >
                        <polyline points="20 6 9 17 4 12"></polyline>
                    </svg>
                </div>
                <div className="exp-date-text">{this.props.expDate}</div>
                <div className="food-name-text">{this.props.foodName}</div>
            </div>
        );
    }
}

class AddFoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedInd: 0, open: false };
        this.expBoxClicked = this.expBoxClicked.bind(this);
    }

    getExpTimeBoxValues() {
        return [
            "1 week",
            "2 weeks",
            "3 weeks",
            "1 month",
            "2 months",
            "3 months"
        ];
    }

    expBoxClicked(index) {
        this.setState({ selectedInd: index });
    }

    handleXClick() {
        const wrapper = document.getElementById("add-food-wrapper");
        wrapper.classList.toggle("add-food-wrapper-open");

        // TODO handle clearing form/reseting selected ind when closing

        this.setState({ open: !this.state.open });
    }

    handleInputFocus() {
        console.log("greetings");
        if (!this.state.open) {
            this.handleXClick();
        }
    }

    render() {
        return (
            <div id="add-food-wrapper" className="add-food-wrapper">
                <div className="x-bg" onClick={() => this.handleXClick()}>
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="3"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-x"
                    >
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                </div>
                <div className="add-food-wrapper-no-x">
                    <input
                        type="text"
                        name="food-to-add"
                        placeholder="Add food..."
                        onFocus={() => this.handleInputFocus()}
                    />
                    <div className="expires-in-text">
                        Expires in{" "}
                        {this.getExpTimeBoxValues()[this.state.selectedInd]}
                    </div>
                    <div className="expire-boxes-wrapper">
                        {this.getExpTimeBoxValues().map((text, index) => (
                            <ExpirationTimeBox
                                text={text}
                                state={
                                    this.state.selectedInd === index
                                        ? "selected"
                                        : "unselected"
                                }
                                index={index}
                                expBoxClicked={this.expBoxClicked}
                                key={text}
                            />
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

class ExpirationTimeBox extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        this.props.expBoxClicked(this.props.index);
    }

    render() {
        return (
            <div
                className={
                    "exp-time-box " +
                    (this.props.state === "unselected"
                        ? "exp-time-unselected"
                        : "exp-time-selected")
                }
                onClick={this.handleClick}
            >
                {this.props.text}
            </div>
        );
    }
}

export default FoodList;
