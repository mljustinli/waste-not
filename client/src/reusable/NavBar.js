import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class NavBar extends Component {
    render() {
        return (
            <div id="header">
                <div id="website-title">Waste</div>
                <div id="nav-link-wrapper">
                    <NavLink exact activeClassName="nav-link-active" to="/">
                        Home
                    </NavLink>
                    <NavLink activeClassName="nav-link-active" to="/about">
                        About
                    </NavLink>
                    <NavLink
                        id="dashboard-link"
                        activeClassName="nav-link-active"
                        to="/dashboard"
                    >
                        Dashboard
                    </NavLink>
                </div>
            </div>
        );
    }
}

export default NavBar;
